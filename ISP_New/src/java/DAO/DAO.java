/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import context.DBContext;
import entity.Users;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultil.MD5;
import ultil.SendMail;

/**
 *
 * @author ducanh
 */
public class DAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public String registerUser(String username, String pass, String birth, String email, String name) {
        try {
            String key = generateRandomString(10);

            String password = MD5.getMd5(pass);
            //chuan bi string sql
            String sql = "insert into Users (username, password, fullname, dob, email, is_active)\n"
                    + "values (?,?,?,?,?,0)";
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(sql);
            //set bien dungs voiw thuw tu bien trong string tren
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, name);
            ps.setString(4, birth);
            ps.setString(5, email);

            //goi cau lenh execute
            ps.executeUpdate();
            SendMail.send(email, "Verify new user", "<h2>Welcome to my system</h2>"
                    + "<a href=\"http://localhost:8080/ISP_New/verify-user?username="
                    + username + "&key=" + key + " \" > Click here to verify your account!</a> ");
            return key;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String generateRandomString(int length) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuilder randomString = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(characters.length());
            char randomChar = characters.charAt(randomIndex);
            randomString.append(randomChar);
        }

        return randomString.toString();
    }

    public boolean accAccount(String username) {
        try {
            String sql = " update Users set is_active = 1 where username = ?";
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.executeUpdate();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Users login(String user, String pass) {
        String sql = "SELECT * FROM Users WHERE username = ? AND password = ? AND is_active = TRUE";
        try {
            conn = new DBContext().getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, pass);
            rs = ps.executeQuery();
            while (rs.next()) {
                Users a = new Users();
                a.setUsername(rs.getString(2));
                a.setPassword(pass);
                return a;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public boolean checkUserExists(String username) {
        String sql = "SELECT COUNT(*) FROM Users WHERE username = ?";
        try {

            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public boolean checkEmailExists(String email) {
        String sql = "SELECT COUNT(*) FROM Users WHERE email = ?";
        try {
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }
    // UserDAO.java

//    public boolean check(String user, String pass) {
//        String sql = "SELECT * FROM Users WHERE username = ? AND password = ? AND is_active = 1";
//        try {
//            conn = new DBContext().getConnection();
//            ps = conn.prepareStatement(sql);
//            ps.setString(1, user);
//            ps.setString(2, MD5.getMd5(pass));
//
//            ResultSet rs = ps.executeQuery(); // Use executeQuery() for SELECT queries
//
//            return rs.next(); // If there's a next row, the username and password match
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            // Close resources in a finally block
//            try {
//                if (ps != null) {
//                    ps.close();
//                }
//                if (conn != null) {
//                    conn.close();
//                }
//            } catch (SQLException ex) {
//                ex.printStackTrace();
//            }
//        }
//        return false;
//    }

    public int changepass(Users a) {
        int n = 0;
        try {
            String sql = "update Users set password=? where username=?";
            
            ps = conn.prepareStatement(sql);
            ps.setString(1, a.getPassword());
            ps.setString(2, a.getUsername());
            n=ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public static void main(String[] args) {
       DAO d = new DAO();
        Users user = d.login("quochuy10", MD5.getMd5("12345678"));
        System.out.println(user);
        
      user.setPassword(MD5.getMd5("123456789"));
       int n = d.changepass(user);
      System.out.println(n);
//        System.out.println("abc");
    }
}
