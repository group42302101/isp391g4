<%-- 
    Document   : login
    Created on : Jan 25, 2024, 12:11:47 PM
    Author     : ducanh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- Thêm đoạn này để hiển thị thông báo -->
        <% if (request.getAttribute("mess") != null) { %>
            <p style="color: red;"><%= request.getAttribute("mess") %></p>
        <% } %>

        <form action="login" method="POST">
            Username:<input type="text" name="username" /><br>
            Password:<input type="password" name="password"  /><br> <!-- Đổi type thành password -->
            <input type="submit" value="Login" />
        </form>
    </body>
</html>
