<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% if (request.getAttribute("error") != null) { %>
            <p style="color: red;"><%= request.getAttribute("error") %></p>
        <% } %>

        <form action="Register" method="POST">
            Username: <input type="text" name="username"  /><br>
            Password: <input type="password" name="pass" /><br>
            Fullname: <input type="text" name="fullname" /><br>
            DOB: <input type="date" name="dob"  /><br>
            Email: <input type="text" name="email"/><br>
            <input type="submit" value="Register" />
        </form>
    </body>
</html>
